### Desafio 1 - Itaú

Olá! :)\
Gostaria muito de agradecer a oportunidade de participar do processo do Itaú!\
Vou explicar mais ou menos como foi o processo de confecção desse primeiro desafio.

### Webpack e Deploy

Utilizei o webpack-dev-server pra não só facilitar o desenvolvimento, pro Browser sempre detectar as mudanças e dar refresh, como também para gerar o Webpack para publicação,
que pode ser encontrado na pasta dist.\
Para roda-lo, é só rodar o `npm run start`, ou abrir diretamente os arquivos html, seja da pasta dist ou da pasta public.\
Tive alguns problemas pra dar deploy pelo heroku, então por comodidade subi o webpack no Netlify e ele pode ser acessado através [desse link](https://alinesantos-itaud1.netlify.com/)

### Ideia geral

Estruturei as pastas e os arquivos de maneira a facilitar caso supostamente o projeto escalasse.
Criei duas classes bases (Month e Expense) para gerenciar as informações da tabela estática e também há funções exportadas na pasta de utils para o desenvolvimento.
O restante da sequencia lógica pode ser localizada no index.js, e cada função possui um comentário em cima, descrevendo mais ou menos o que eu tinha em mente quando
desenhei o algoritmo.

É isso!\
Aguardando retorno. :)\
Bora pro próximo desafio?\
\
*Aline de Lima Santos*